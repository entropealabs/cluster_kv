# ClusterKV

An ephemeral distributed KV store or [DHT](https://en.wikipedia.org/wiki/Distributed_hash_table)

Backed by in-memory ETS table.

Utilizes [libcluster](https://github.com/bitwalker/libcluster) for clustering 

and [libring](https://github.com/bitwalker/libring) for the consistent hash ring.

See [cluster_test](https://gitlab.com/entropealabs/cluster_test) for an example utilization

Primarily built for use within a distributed WAMP Router. See [WAMPex](https://gitlab.com/entropealabs/wampex) for more information and [wamp-proto.org](https://wamp-proto.org/) for more information about the WAMP protocol

## Installation

```elixir
def deps do
  [
    {:cluster_kv, git: "https://gitlab.com/entropealabs/cluster_kv", tag: "e042a674095208de84a86c32239cecc9834d0177"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc).

