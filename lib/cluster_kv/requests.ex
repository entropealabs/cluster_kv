defmodule ClusterKV.Requests do
  @moduledoc """
  Type and functions for working with requests
  """
  alias ClusterKV.DB

  @type t ::
          {reference :: reference(), from :: GenServer.from(), nodes :: [node()],
           responses :: [DB.element()]}

  @spec get(reference :: reference(), requests :: [t()]) :: t() | nil
  def get(ref, reqs) do
    Enum.find(reqs, fn
      {^ref, _, _, _} -> true
      _ -> false
    end)
  end

  @spec remove_node(requests :: [t()], node :: node()) :: [t()]
  def remove_node(reqs, node) do
    Enum.map(reqs, fn {ref, fr, nodes, res} ->
      {ref, fr, List.delete(nodes, node), res}
    end)
  end

  @spec decrement(
          reference :: reference(),
          requests :: [t()],
          value :: any(),
          from :: node()
        ) :: [t()]
  def decrement(ref, reqs, val, from) do
    Enum.map(reqs, fn
      {^ref, f, c, res} -> {ref, f, List.delete(c, from), [val | res]}
      other -> other
    end)
  end

  @spec remove(reference :: reference(), requests :: [t()]) :: [t()]
  def remove(ref, requests) do
    Enum.filter(requests, fn
      {^ref, _, _, _} -> false
      {_, _, _, _} -> true
    end)
  end
end
