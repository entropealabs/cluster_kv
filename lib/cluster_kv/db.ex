defmodule ClusterKV.DB do
  @moduledoc false
  use GenServer
  require Logger

  alias __MODULE__

  defstruct [:db, :batch_chunk, :batch_fun, :batch_ref, last_batch: 0, batch: []]

  @type element :: {key :: String.t(), value :: any()} | :not_found

  @type update_function ::
          :diff
          | :not_in
          | :keystore
          | :replace
          | :replace_list
          | :last_30
          | :unique
          | :remove
          | :round_robin
          | fun()

  @type t :: %__MODULE__{
          db: :ets.tid() | atom(),
          batch: [element()],
          batch_chunk: integer(),
          batch_fun: fun(),
          batch_ref: reference(),
          last_batch: integer()
        }

  @spec get(name :: module(), key :: binary(), timeout :: non_neg_integer() | :infinity) ::
          element() | :not_found
  def get(name, key, timeout \\ :infinity) do
    :poolboy.transaction(name, fn w ->
      GenServer.call(w, {:get, key}, timeout)
    end)
  end

  @spec update_if(
          name :: module(),
          key :: binary(),
          value :: any(),
          fun :: update_function(),
          timeout :: non_neg_integer() | :infinity
        ) ::
          element() | :not_found
  def update_if(name, key, value, fun, timeout \\ :infinity) do
    :poolboy.transaction(name, fn w ->
      GenServer.call(w, {:update_if, key, value, fun}, timeout)
    end)
  end

  @spec put(name :: module(), key :: binary(), value :: term()) :: :ok
  def put(name, key, value) do
    :poolboy.transaction(name, fn w ->
      GenServer.cast(w, {:put, key, value})
    end)
  end

  @spec upsert(name :: module(), key :: binary(), value :: term(), fun :: update_function()) ::
          :ok
  def upsert(name, key, value, fun \\ &{elem(&1, 0), Enum.uniq([&2 | elem(&1, 1)])}) do
    :poolboy.transaction(name, fn w ->
      GenServer.cast(w, {:upsert, key, value, fun})
    end)
  end

  @spec batch(
          name :: module(),
          batch :: [element()],
          chunk :: integer(),
          fun :: update_function()
        ) :: :ok
  def batch(name, batch, chunk \\ 10, fun \\ &{elem(&1, 0), Enum.uniq(elem(&1, 1) ++ &2)}) do
    :poolboy.transaction(name, fn w ->
      GenServer.cast(w, {:batch, batch, chunk, fun})
    end)
  end

  @spec stream(name :: module(), timeout :: non_neg_integer() | :infinity) :: Enumerable.t()
  def stream(name, timeout \\ :infinity) do
    :poolboy.transaction(name, fn w ->
      GenServer.call(w, :stream, timeout)
    end)
  end

  @spec start_link(table: :ets.tid() | atom()) :: GenServer.on_start()
  def start_link(table: table) do
    GenServer.start_link(__MODULE__, table)
  end

  def init(table) do
    {:ok, %DB{db: table}}
  end

  def handle_call(:stream, _from, %DB{db: db} = state) do
    {:reply, get_stream(db), state}
  end

  def handle_call({:get, key}, _from, %DB{db: db} = state) do
    Logger.debug("DB handling GET #{key}")
    {:reply, do_get(db, key), state}
  end

  def handle_call({:update_if, key, value, fun}, _from, %DB{db: db} = state) do
    Logger.debug("DB handling UPDATE_IF #{key}")
    {:reply, do_update_if(db, key, value, fun), state}
  end

  def handle_cast({:put, key, value}, %DB{db: db} = state) do
    Logger.debug("DB handling PUT #{key}")
    :ets.insert(db, {key, [value]})
    {:noreply, state}
  end

  def handle_cast({:upsert, key, value, fun}, %DB{db: db} = state) do
    Logger.debug("DB handling UPSERT #{key}")
    do_upsert(db, key, value, fun)
    {:noreply, state}
  end

  def handle_cast({:batch, batch, chunk, fun}, %DB{batch_ref: ref, batch: b} = state) do
    Logger.debug("DB handling BATCH")
    batch = b ++ batch

    ref =
      case ref do
        nil -> Process.send_after(self(), :process_batch, 0)
        r -> r
      end

    {:noreply, %DB{state | batch_ref: ref, batch: batch, batch_chunk: chunk, batch_fun: fun}}
  end

  def handle_info(
        :process_batch,
        %DB{
          db: db,
          batch_chunk: chunk,
          batch_fun: fun,
          batch: batch,
          last_batch: lb
        } = state
      ) do
    {batch, lb, ref} = handle_next_batch_chunk(db, batch, chunk, lb, fun)
    {:noreply, %DB{state | batch_ref: ref, batch: batch, last_batch: lb}}
  end

  @spec handle_next_batch_chunk(
          db :: reference(),
          batch :: [element()],
          chunk :: integer(),
          last_batch :: integer(),
          fun :: update_function()
        ) :: {[element()], integer(), reference()}
  defp handle_next_batch_chunk(db, batch, chunk, last_batch, fun) do
    Logger.debug("processing batch of #{length(batch)} from #{last_batch} with chunk of #{chunk}")

    batch
    |> Enum.slice(last_batch, chunk)
    |> Enum.each(fn {k, v} ->
      do_upsert(db, k, v, fun)
    end)

    case last_batch + chunk do
      lb when lb > length(batch) ->
        Logger.debug("Batch Processed")
        {[], 0, nil}

      lb ->
        ref = Process.send_after(self(), :process_batch, 0)
        {batch, lb, ref}
    end
  end

  @spec do_upsert(db :: module(), key :: String.t(), value :: any(), fun :: update_function()) ::
          true
  defp do_upsert(db, key, value, fun) do
    get_lock(db, key)
    fun = get_update_function(fun)

    case :ets.lookup(db, key) do
      [] ->
        v =
          case value do
            val when is_list(val) -> val
            v -> [v]
          end

        :ets.insert(db, {key, v})

      [{_, val} = old] when is_list(val) ->
        new = fun.(old, value)
        :ets.insert(db, new)

      [_] ->
        :ets.insert(db, {key, [value]})
    end

    release_lock(db, key)
  end

  @spec do_update_if(db :: module(), key :: String.t(), value :: any(), fun :: update_function()) ::
          :updated | :noop
  defp do_update_if(db, key, value, fun) do
    get_lock(db, key)
    fun = get_update_function(fun)

    case :ets.lookup(db, key) do
      [] ->
        v =
          case value do
            val when is_list(val) -> val
            v -> [v]
          end

        :ets.insert(db, {key, v})
        release_lock(db, key)
        :updated

      [{_, val} = old] when is_list(val) ->
        case fun.(old, value) do
          {:update, new} ->
            :ets.insert(db, new)
            release_lock(db, key)
            :updated

          _ ->
            release_lock(db, key)
            :noop
        end

      [_] ->
        :ets.insert(db, {key, [value]})
        release_lock(db, key)
        :updated
    end
  end

  defp get_update_function(:diff), do: &diff/2
  defp get_update_function(:not_in), do: &not_in/2
  defp get_update_function(:keystore), do: &keystore/2
  defp get_update_function(:replace), do: &replace/2
  defp get_update_function(:replace_list), do: &replace_list/2
  defp get_update_function(:last_30), do: &last_30/2
  defp get_update_function(:unique), do: &unique/2
  defp get_update_function(:remove), do: &remove/2
  defp get_update_function(:round_robin), do: &round_robin/2
  defp get_update_function(fun) when is_function(fun), do: fun
  defp get_update_function(_), do: fn _, _ -> :noop end

  defp diff({key, [old]}, val) do
    case old == val do
      true -> :noop
      false -> {:update, {key, [val]}}
    end
  end

  defp not_in({key, old_vals}, val) do
    case val in old_vals do
      true -> :noop
      false -> {:update, {key, Enum.slice([val | old_vals], 0..10)}}
    end
  end

  defp keystore({k, old}, {key, _} = new), do: {k, List.keystore(old, key, 0, new)}
  defp replace({k, _old}, value), do: {k, [value]}
  defp replace_list({k, _old}, value), do: {k, value}
  defp last_30({k, old}, value), do: {k, [value | old] |> Enum.take(30)}
  defp unique({k, old}, value), do: {k, [value | old] |> Enum.uniq()}
  defp remove({k, values}, value), do: {k, List.delete(values, value)}

  defp round_robin({id, v}, value) do
    val =
      case v do
        [values] -> values
        [_ | t] -> Enum.reduce(t, 0, fn a, _acc -> a end)
      end

    case val + 1 do
      ni when ni < value ->
        {id, [ni]}

      _ ->
        {id, [0]}
    end
  end

  @spec do_get(db :: module(), key :: String.t()) :: element() | :not_found
  defp do_get(db, key) do
    case :ets.lookup(db, key) do
      [] -> :not_found
      [other] -> other
    end
  end

  @spec get_stream(db :: module()) :: Enumerable.t()
  defp get_stream(db) do
    :ets.safe_fixtable(db, true)

    db
    |> :ets.first()
    |> Stream.iterate(&:ets.next(db, &1))
    |> Stream.take_while(fn
      :"$end_of_table" ->
        :ets.safe_fixtable(db, false)
        false

      _ ->
        true
    end)
    |> Stream.map(fn key ->
      do_get(db, key)
    end)
  end

  defp release_lock(db, key) do
    :ets.delete(db, "#{key}:lock")
  end

  defp get_lock(db, key) do
    key = "#{key}:lock"
    do_get_lock(db, key)
  end

  defp do_get_lock(db, key) do
    case :ets.insert_new(db, {key, true}) do
      false -> do_get_lock(db, key)
      true -> true
    end
  end
end
