defmodule ClusterKV.Ring do
  @moduledoc false
  @external_resource "priv/ring.json"
  use StatesLanguage, data: "priv/ring.json"
  require Logger
  alias StatesLanguage, as: SL
  alias ClusterKV.{DB, Requests, Ring}

  @enforce_keys [:name, :replicas, :quorum, :db]
  defstruct [
    :name,
    :replicas,
    :quorum,
    :ring,
    :db,
    :node,
    requests: [],
    anti_entropy_interval: 5 * 60_000
  ]

  @type t :: %__MODULE__{
          replicas: integer(),
          quorum: integer(),
          db: module(),
          node: node(),
          ring: HashRing.t(),
          requests: [Requests.t()],
          anti_entropy_interval: integer()
        }

  # States
  @init "Init"
  @ready "Ready"
  @quorum "Quorum"

  # Resources
  @handle_init "HandleInit"
  @handle_ready "HandleReady"
  @await_quorum "AwaitQuorum"
  @handle_node_up "HandleNodeUp"
  @handle_node_down "HandleNodeDown"

  @spec put(name :: module(), keyspace :: String.t(), key :: String.t(), value :: term()) :: :ok
  def put(name, keyspace, key, value) do
    :gen_statem.cast(ClusterKV.ring_name(name), {:put, keyspace, key, value})
  end

  @spec update(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          value :: any(),
          fun :: DB.update_function()
        ) :: :ok
  def update(name, keyspace, key, value, fun) do
    :gen_statem.cast(ClusterKV.ring_name(name), {:update, keyspace, key, value, fun})
  end

  @spec update_if(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          value :: any(),
          fun :: DB.update_function()
        ) :: :updated | :noop
  def update_if(name, keyspace, key, value, fun) do
    :gen_statem.call(ClusterKV.ring_name(name), {:update_if, keyspace, key, value, fun})
  end

  @spec get_wildcard_key(
          key :: String.t(),
          split_on :: String.t(),
          join :: String.t(),
          wildcard :: String.t()
        ) :: :ok
  def get_wildcard_key(key, split_on, join, wildcard) do
    do_get_wildcard_key(key, split_on, wildcard, join)
  end

  @spec put_wildcard(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          value :: term(),
          split_on :: String.t(),
          join :: String.t(),
          wildcard :: String.t()
        ) :: :ok
  def put_wildcard(name, keyspace, key, value, split_on, join, wildcard) do
    :gen_statem.cast(
      ClusterKV.ring_name(name),
      {:put_wildcard, keyspace, key, value, split_on, join, wildcard}
    )
  end

  @spec batch(name :: module(), keyspace :: String.t(), batch :: [DB.element()]) :: :ok
  def batch(name, keyspace, batch) do
    :gen_statem.cast(ClusterKV.ring_name(name), {:batch, keyspace, batch})
  end

  @spec get(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          timeout :: non_neg_integer() | :infinity
        ) ::
          DB.element() | :not_found
  def get(name, keyspace, key, timeout) do
    :gen_statem.call(ClusterKV.ring_name(name), {:get, keyspace, key}, timeout)
  end

  @spec prefix(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          split_on :: String.t(),
          min :: integer(),
          timeout :: non_neg_integer() | :infinity
        ) :: [DB.element()] | :not_found
  def prefix(name, keyspace, key, split_on, min, timeout) do
    :gen_statem.call(ClusterKV.ring_name(name), {:prefix, keyspace, key, split_on, min}, timeout)
  end

  @spec wildcard(
          name :: module(),
          keyspace :: String.t(),
          key :: String.t(),
          split_on :: String.t(),
          join :: String.t(),
          wildcard :: String.t(),
          timeout :: non_neg_integer() | :infinity
        ) :: [DB.element()] | :not_found
  def wildcard(name, keyspace, key, split_on, join, wildcard, timeout) do
    :gen_statem.call(
      ClusterKV.ring_name(name),
      {:wildcard, keyspace, key, split_on, join, wildcard},
      timeout
    )
  end

  @spec stream(name :: module(), timeout :: non_neg_integer() | :infinity) :: Enumerable.t()
  def stream(name, timeout) do
    :gen_statem.call(ClusterKV.ring_name(name), :stream, timeout)
  end

  def handle_resource(@handle_init, _, @init, %SL{data: %Ring{} = data} = sl) do
    node = Node.self()

    ring =
      node
      |> HashRing.new()
      |> get_existing_nodes()

    :net_kernel.monitor_nodes(true, node_type: :all)

    {:ok, %SL{sl | data: %Ring{data | node: node, ring: ring}},
     [{:next_event, :internal, :initialized}]}
  end

  def handle_resource(@await_quorum, _, @quorum, %SL{data: %Ring{quorum: q, ring: r}} = sl) do
    actions =
      case length(HashRing.nodes(r)) >= q do
        true -> [{:next_event, :internal, :quorum}]
        false -> []
      end

    {:ok, sl, actions}
  end

  def handle_resource(
        @handle_ready,
        _,
        @ready,
        sl
      ) do
    {:ok, sl, []}
  end

  def handle_resource(@handle_node_up, _, _, sl) do
    {:ok, sl, [{:next_event, :internal, :node_added}]}
  end

  def handle_resource(@handle_node_down, _, _, sl) do
    {:ok, sl, [{:next_event, :internal, :node_removed}]}
  end

  def handle_call(
        :stream,
        from,
        @ready,
        %SL{data: %Ring{db: db}} = sl
      ) do
    {:ok, sl, [{:reply, from, DB.stream(db)}]}
  end

  def handle_call(
        {:get, keyspace, key},
        from,
        @ready,
        %SL{data: %Ring{name: n, node: me, requests: reqs, ring: r, replicas: repls} = data} = sl
      ) do
    key = "#{keyspace}:#{key}"
    node = get_node(key, r, me, repls)
    ref = make_ref()
    send({n, node}, {:get_key, key, ref, me})
    {:ok, %SL{sl | data: %Ring{data | requests: [{ref, from, [node], []} | reqs]}}, []}
  end

  def handle_call(
        {:update_if, keyspace, key, value, fun},
        from,
        @ready,
        %SL{data: %Ring{name: n, node: me, requests: reqs, ring: r} = data} = sl
      ) do
    key = "#{keyspace}:#{key}"
    node = HashRing.key_to_node(r, key)
    ref = make_ref()
    send({n, node}, {:update_if, key, value, fun, ref, me})
    {:ok, %SL{sl | data: %Ring{data | requests: [{ref, from, [node], []} | reqs]}}, []}
  end

  def handle_call({:get, _keyspace, _key}, from, _, sl),
    do: {:ok, sl, [{:reply, from, :no_quorum}]}

  def handle_call(
        {:prefix, keyspace, key, split_on, min},
        from,
        @ready,
        %SL{data: %Ring{name: n, node: me, requests: reqs, ring: r, replicas: repls} = data} = sl
      ) do
    ref = make_ref()
    key = "#{keyspace}:#{key}"
    parts = String.split(key, split_on)
    head = Enum.slice(parts, 0..(min - 1))
    itr = Enum.slice(parts, min..-2//1)
    node = get_prefix(head, r, me, repls, n, ref)

    {_, nodes} =
      Enum.reduce(itr, {head, [node]}, fn ns, {acc, nodes} ->
        next = acc ++ [ns]
        node = get_prefix(next, r, me, repls, n, ref)
        {next, [node | nodes]}
      end)

    {:ok, %SL{sl | data: %Ring{data | requests: [{ref, from, nodes, []} | reqs]}}, []}
  end

  def handle_call({:prefix, _, _, _, _}, from, _, sl), do: {:ok, sl, [{:reply, from, :no_quorum}]}

  def handle_call(
        {:wildcard, keyspace, key, split_on, join, wildcard},
        from,
        @ready,
        %SL{data: %Ring{name: n, node: me, requests: reqs, ring: r, replicas: repls} = data} = sl
      ) do
    ref = make_ref()

    parts = String.split(key, split_on)

    nodes =
      Enum.map(Enum.with_index(parts), fn {k, i} ->
        k = Enum.join([keyspace, k, i], join)
        get_wildcard(k, parts, wildcard, r, me, repls, n, ref)
      end)

    {:ok, %SL{sl | data: %Ring{data | requests: [{ref, from, nodes, []} | reqs]}}, []}
  end

  def handle_call(_, from, _, sl), do: {:ok, sl, [{:reply, from, :no_quorum}]}

  def handle_cast(
        {:batch, keyspace, batch},
        _,
        %SL{data: %Ring{name: n, ring: r, replicas: repls}} = sl
      ) do
    batches =
      Enum.reduce(batch, %{}, fn {k, v}, acc ->
        k = "#{keyspace}:#{k}"
        nodes = HashRing.key_to_nodes(r, k, repls)
        el = {k, v}

        Enum.reduce(nodes, acc, fn n, a ->
          Map.update(a, n, [el], &[el | &1])
        end)
      end)

    send_batch(n, batches)
    {:ok, sl, []}
  end

  def handle_cast(
        {:put_wildcard, keyspace, key, value, split_on, join, wildcard},
        _,
        %SL{data: %Ring{name: n, ring: r, replicas: repls}} = sl
      ) do
    key
    |> String.split(split_on)
    |> send_wildcard(keyspace, wildcard, join, value, n, r, repls)

    {:ok, sl, []}
  end

  def handle_cast(
        {:put, keyspace, key, value},
        _,
        %SL{data: %Ring{name: n, ring: r, replicas: repls}} = sl
      ) do
    key = "#{keyspace}:#{key}"
    nodes = HashRing.key_to_nodes(r, key, repls)
    send_sync(n, nodes, key, value)
    {:ok, sl, []}
  end

  def handle_cast(
        {:update, keyspace, key, value, fun},
        _,
        %SL{data: %Ring{name: n, ring: r, replicas: repls}} = sl
      ) do
    key = "#{keyspace}:#{key}"
    nodes = HashRing.key_to_nodes(r, key, repls)
    send_update(n, nodes, key, value, fun)
    {:ok, sl, []}
  end

  def handle_cast({:put, _keyspace, _key, _value}, _, sl), do: {:ok, sl, []}

  def handle_cast(_, _, sl), do: {:ok, sl, []}

  def handle_info({:sync, key, value}, _, %SL{data: %Ring{db: db}} = sl) do
    DB.upsert(db, key, value)
    {:ok, sl, []}
  end

  def handle_info({:update, key, value, fun}, _, %SL{data: %Ring{db: db}} = sl) do
    DB.upsert(db, key, value, fun)
    {:ok, sl, []}
  end

  def handle_info({:get_key, key, ref, node}, _, %SL{data: %Ring{name: n, db: db, node: me}} = sl) do
    send({n, node}, {:reply, get_key(db, key), ref, me})
    {:ok, sl, []}
  end

  def handle_info(
        {:update_if, key, value, fun, ref, node},
        _,
        %SL{data: %Ring{name: n, db: db, node: me}} = sl
      ) do
    send({n, node}, {:reply, DB.update_if(db, key, value, fun), ref, me})
    {:ok, sl, []}
  end

  def handle_info(
        {:get_wildcard, key, parts, wildcard, ref, node},
        _,
        %SL{data: %Ring{name: n, db: db, node: me}} = sl
      ) do
    val =
      case get_key(db, key) do
        :not_found ->
          :not_found

        {_k, vals} ->
          parts = Enum.with_index(parts)

          Enum.filter(vals, fn {p, _val} ->
            all_wildcard(parts, p, wildcard)
          end)
      end

    send({n, node}, {:reply, val, ref, me})
    {:ok, sl, []}
  end

  def handle_info({:reply, val, ref, from}, _, %SL{data: %Ring{requests: reqs} = data} = sl) do
    {requests, actions} = maybe_reply(ref, reqs, val, from)
    {:ok, %SL{sl | data: %Ring{data | requests: requests}}, actions}
  end

  def handle_info(
        {:nodeup, node, info},
        _,
        %SL{data: %Ring{name: n, node: me, db: db, ring: r, replicas: repls} = data} = sl
      ) do
    Logger.info("Nodeup: #{inspect(node)} - #{inspect(info)}")
    ring = HashRing.add_node(r, node)
    redistribute_data(n, me, db, r, ring, repls)
    {:ok, %SL{sl | data: %Ring{data | ring: ring}}, [{:next_event, :internal, :node_up}]}
  end

  def handle_info(
        {:nodedown, node, info},
        _,
        %SL{
          data: %Ring{name: n, requests: reqs, node: me, db: db, ring: r, replicas: repls} = data
        } = sl
      ) do
    Logger.info("Nodedown: #{inspect(node)} - #{inspect(info)}")
    ring = HashRing.remove_node(r, node)
    redistribute_data(n, me, db, r, ring, repls)
    reqs = Requests.remove_node(reqs, node)

    {requests, actions} =
      Enum.reduce(reqs, {reqs, [{:next_event, :internal, :node_down}]}, fn {ref, _fr, _nodes,
                                                                            _res},
                                                                           {rs, acts} ->
        {requests, actions} = maybe_reply(ref, rs, :not_found, node)
        {requests, actions ++ acts}
      end)

    {:ok, %SL{sl | data: %Ring{data | ring: ring, requests: requests}}, actions}
  end

  def handle_info({:handle_batch, batch}, _, %SL{data: %Ring{db: db}} = sl) do
    DB.batch(db, batch)
    {:ok, sl, []}
  end

  def handle_info(e, s, sl) do
    Logger.warning("Unknown Info Event: #{inspect(e)} in state #{s} with data #{inspect(sl)}")
    {:ok, sl, []}
  end

  defp all_wildcard(parts, p, wildcard) do
    Enum.all?(parts, fn {k, i} ->
      case Enum.at(p, i) do
        ^k -> true
        ^wildcard -> true
        _ -> false
      end
    end)
  end

  @spec get_prefix(
          next :: [String.t()],
          ring :: HashRing.t(),
          me :: node(),
          replicas :: non_neg_integer(),
          name :: module(),
          reference :: reference()
        ) :: node()
  defp get_prefix(next, r, me, repls, n, ref) do
    prefix = Enum.join(next, ".")
    node = get_node(prefix, r, me, repls)
    send({n, node}, {:get_key, prefix, ref, me})
    node
  end

  def get_key(db, key) do
    case DB.get(db, key) do
      :not_found ->
        :not_found

      {id, val} ->
        [_keyspace, key] = String.split(id, ":", parts: 2)
        {key, val}
    end
  end

  @spec get_wildcard(
          key :: String.t(),
          parts :: [String.t()],
          wildcard :: String.t(),
          ring :: HashRing.t(),
          me :: node(),
          replicas :: non_neg_integer(),
          name :: module(),
          ref :: reference()
        ) :: node()
  defp get_wildcard(key, parts, wildcard, r, me, repls, n, ref) do
    node = get_node(key, r, me, repls)
    send({n, node}, {:get_wildcard, key, parts, wildcard, ref, me})
    node
  end

  @spec send_batch(name :: module(), batches :: map()) :: :ok
  def send_batch(name, batches) do
    Logger.debug("Sending batches to: #{inspect(Map.keys(batches))}")

    Enum.each(batches, fn {node, batch} ->
      Logger.debug("Sending #{length(batch)} records to #{inspect(node)}")
      send({name, node}, {:handle_batch, batch})
    end)
  end

  @spec send_wildcard(
          parts :: [String.t()],
          header :: String.t(),
          wildcard :: String.t(),
          join :: String.t(),
          value :: any(),
          name :: module(),
          ring :: HashRing.t(),
          replicas :: non_neg_integer()
        ) :: [:ok]
  def send_wildcard(parts, header, wildcard, join, value, name, ring, repls) do
    parts
    |> Enum.with_index()
    |> Enum.take_while(fn
      {^wildcard, _} ->
        true

      {k, i} ->
        k = Enum.join([header, k, i], join)
        nodes = HashRing.key_to_nodes(ring, k, repls)
        send_sync(name, nodes, k, {parts, value})
        false
    end)
  end

  defp do_get_wildcard_key(topic, split_on, wildcard, join) do
    topic
    |> String.split(split_on)
    |> Enum.with_index()
    |> Enum.reduce_while(nil, fn
      {^wildcard, _}, acc ->
        {:cont, acc}

      {k, i}, _ ->
        {:halt, Enum.join([k, i], join)}
    end)
  end

  @spec redistribute_data(
          name :: module(),
          me :: node(),
          db :: module(),
          old_ring :: HashRing.t(),
          new_ring :: HashRing.t(),
          replicas :: non_neg_integer()
        ) :: {:ok, pid()}
  def redistribute_data(name, me, db, old_ring, new_ring, repls) do
    Task.start(fn ->
      batch =
        db
        |> DB.stream()
        |> Enum.reduce(%{}, fn {k, v}, acc ->
          old_nodes = HashRing.key_to_nodes(old_ring, k, repls) |> MapSet.new()

          new_ring
          |> HashRing.key_to_nodes(k, repls)
          |> MapSet.new()
          |> MapSet.difference(old_nodes)
          |> MapSet.to_list()
          |> reduce_nodes(me, k, v, acc)
        end)

      send_batch(name, batch)
    end)
  end

  defp reduce_nodes(nodes, me, k, v, acc) do
    Enum.reduce(nodes, acc, fn
      ^me, a ->
        a

      n, a ->
        Map.update(a, n, [], &[{k, v} | &1])
    end)
  end

  @spec maybe_reply(ref :: reference(), requests :: [Requests.t()], val :: any(), node :: node()) ::
          {[Requests.t()], [:gen_statem.action()]}
  defp maybe_reply(ref, reqs, val, node) do
    case Requests.get(ref, reqs) do
      {_, from, [_], res} ->
        {Requests.remove(ref, reqs), [{:reply, from, get_final_value(res, val)}]}

      {_, from, [], res} ->
        {Requests.remove(ref, reqs), [{:reply, from, get_final_value(res, val)}]}

      _ ->
        {Requests.decrement(ref, reqs, val, node), []}
    end
  end

  @spec get_final_value(result :: [DB.element()], value :: any()) :: [DB.element()] | DB.element()
  defp get_final_value([], val), do: val

  defp get_final_value(res, val) when is_list(res),
    do:
      [val | res]
      |> List.flatten()
      |> Enum.filter(fn
        :not_found -> false
        _ -> true
      end)

  @spec send_sync(
          name :: module(),
          nodes :: [node()] | {:error, {:invalid_ring, :no_nodes}},
          key :: binary(),
          value :: any()
        ) :: :ok
  defp send_sync(_name, {:error, er}, _key, _value) do
    Logger.warning("No nodes to sync with, #{inspect(er)}")
  end

  defp send_sync(name, nodes, key, value) do
    Enum.each(nodes, fn n ->
      send({name, n}, {:sync, key, value})
    end)
  end

  @spec send_update(
          name :: module(),
          nodes :: [node()],
          key :: String.t(),
          value :: any(),
          fun :: any()
        ) :: :ok
  defp send_update(name, nodes, key, value, fun) do
    Enum.each(nodes, fn n ->
      send({name, n}, {:update, key, value, fun})
    end)
  end

  @spec get_existing_nodes(ring :: HashRing.t()) :: HashRing.t()
  defp get_existing_nodes(ring) do
    Enum.reduce(Node.list(), ring, fn n, acc ->
      HashRing.add_node(acc, n)
    end)
  end

  @spec get_node(
          key :: String.t(),
          ring :: HashRing.t(),
          node :: node(),
          replicas :: non_neg_integer()
        ) ::
          node()
  defp get_node(key, r, node, repls) do
    nodes = HashRing.key_to_nodes(r, key, repls)

    case node in nodes do
      true -> node
      false -> Enum.random(nodes)
    end
  end
end
