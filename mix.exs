defmodule ClusterKv.MixProject do
  use Mix.Project

  def project do
    [
      app: :cluster_kv,
      version: "0.1.0",
      elixir: "~> 1.17",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      package: package(),
      description: description(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  def description do
    "An ephemeral distributed KV store"
  end

  def package do
    [
      name: :cluster_kv,
      files: ["lib", "priv", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Christopher Steven Coté"],
      licenses: ["MIT License"],
      links: %{
        "GitLab" => "https://gitlab.com/entropealabs/cluster_kv",
        "Docs" => "https://hexdocs.pm/cluster_kv"
      }
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.7", only: [:test], runtime: false},
      {:dialyxir, "~> 1.4", only: [:dev, :test], runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev, runtime: false},
      {:libcluster, "~> 3.2"},
      {:libring, "~> 1.4"},
      {:poolboy, "~> 1.5"},
      {:states_language, "~> 0.4"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "test",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
